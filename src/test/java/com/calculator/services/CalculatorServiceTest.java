package test.java.com.calculator.services;

import main.java.com.calculator.services.CalculatorService;
import main.java.com.calculator.utils.InputValidation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import static main.java.com.calculator.utils.InputValidation.INCORRECT_INPUT;


import javax.swing.*;
import java.awt.event.WindowEvent;

public class CalculatorServiceTest {
    InputValidation inputValidation = Mockito.mock(InputValidation.class);

    JTextField operand1TextField = new JTextField();
    JTextField operand2TextField = new JTextField();
    JTextField operatorTextField = new JTextField();
    JTextField outputTextField = new JTextField();
    JButton jButton = new JButton();
    CalculatorService cut = new CalculatorService(operand1TextField, operand2TextField, operatorTextField,
            outputTextField, jButton, inputValidation);

    static Arguments[] runTestArgs(){
        return new Arguments[]{
          Arguments.arguments(" 34  ", " 2 2 ", " + ", "34", "22", "+", "56"),
          Arguments.arguments(" 4  5  ", " 12 ", " + ", "45", "12", "-", "33"),
          Arguments.arguments(" 12 2  ", " 2 ", " + ", "122", "2", "/", "61"),
          Arguments.arguments(" 11  ", " 4 ", " + ", "11", "4", "*", "44"),
          Arguments.arguments(" 3ew4  ", " 2 2 ", " + ", INCORRECT_INPUT, "34", "+", INCORRECT_INPUT),
          Arguments.arguments(" 3  ", " 2+2 ", " * ", "3", INCORRECT_INPUT, "*", INCORRECT_INPUT),
          Arguments.arguments(" 2  ", " 1 12 ", " +/ ", "52", "112", INCORRECT_INPUT, INCORRECT_INPUT)
        };
    }

    @ParameterizedTest
    @MethodSource("runTestArgs")
    void runTest(String firstValue, String secondValue, String operator, String firstOperand,
                 String secondOperand, String operatorArg, String expected){
        Mockito.when(inputValidation.operandTransformation(firstValue)).thenReturn(firstOperand);
        Mockito.when(inputValidation.operandTransformation(secondValue)).thenReturn(secondOperand);
        Mockito.when(inputValidation.operatorTransformation(operator)).thenReturn(operatorArg);

        cut.run();

        operand1TextField.setText(firstValue);
        operand2TextField.setText(secondValue);
        operatorTextField.setText(operator);
        jButton.doClick();
        String actual = outputTextField.getText();

        Assertions.assertEquals(expected, actual);
    }
}
