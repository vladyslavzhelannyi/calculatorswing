package test.java.com.calculator.utils;

import main.java.com.calculator.utils.InputValidation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class InputValidationTest {
    InputValidation cut = new InputValidation();

    static Arguments[] operandTransformationTestArgs(){
        return new Arguments[]{
          Arguments.arguments("34", "34"),
          Arguments.arguments("   12     ", "12"),
          Arguments.arguments("  3 5 6   ", "356"),
          Arguments.arguments("abc", InputValidation.INCORRECT_INPUT),
          Arguments.arguments("14O", InputValidation.INCORRECT_INPUT)
        };
    }

    static Arguments[] operatorTransformationTestArgs(){
        return new Arguments[]{
                Arguments.arguments("+", "+"),
                Arguments.arguments("  -   ", "-"),
                Arguments.arguments("34", InputValidation.INCORRECT_INPUT),
                Arguments.arguments("car", InputValidation.INCORRECT_INPUT),
                Arguments.arguments("+-", InputValidation.INCORRECT_INPUT)
        };
    }

    @ParameterizedTest
    @MethodSource("operandTransformationTestArgs")
    void operandTransformationTest(String userInput, String expected){
        String actual = cut.operandTransformation(userInput);
        Assertions.assertEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("operatorTransformationTestArgs")
    void operatorTransformationTest(String userInput, String expected){
        String actual = cut.operatorTransformation(userInput);
        Assertions.assertEquals(expected, actual);
    }
}
