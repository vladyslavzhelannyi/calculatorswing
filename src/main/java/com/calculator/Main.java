package main.java.com.calculator;

import main.java.com.calculator.services.CalculatorService;
import main.java.com.calculator.utils.InputValidation;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        JTextField operand1TextField = new JTextField();
        JTextField operand2TextField = new JTextField();
        JTextField operatorTextField = new JTextField();
        JTextField outputTextField = new JTextField();
        InputValidation inputValidation = new InputValidation();
        JButton startButton = new JButton();
        CalculatorService calculator = new CalculatorService(operand1TextField,
                operand2TextField, operatorTextField, outputTextField, startButton, inputValidation);
        calculator.run();
    }
}
