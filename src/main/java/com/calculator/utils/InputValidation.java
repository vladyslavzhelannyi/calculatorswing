package main.java.com.calculator.utils;

public class InputValidation {
    public static final String INCORRECT_INPUT = "Ввод некорректный";
    private static final String PLUS = "+";
    private static final String MINUS = "-";
    private static final String DIVIDE = "/";
    private static final String MULTIPLY = "*";
    private static final String REMAINDER_OF_DIVISION = "%";
    private static final String RESULT = "Result: ";
    private static final String EXIT = "exit";
    private static final String ZERO = "0";
    private static final String ONE = "1";
    private static final String TWO = "2";
    private static final String THREE = "3";
    private static final String FOUR = "4";
    private static final String FIVE = "5";
    private static final String SIX = "6";
    private static final String SEVEN = "7";
    private static final String EIGHT = "8";
    private static final String NINE = "9";
    private static final String SPACE = " ";


    public String operandTransformation(String userInput){
        String[] userInputSplited = userInput.split("");
        String[] inputWithoutSpace = userInputSplited;
        String operand = "";

        int[] spacesToDeleteIndexes = {};

        boolean hasIncorrectSymbols = false;
        for(int i = 0; i < userInputSplited.length; i++){
            if(userInputSplited[i].equals(SPACE)){
                spacesToDeleteIndexes = addElement(spacesToDeleteIndexes, i);
            }
        }
        for(int i = spacesToDeleteIndexes.length - 1; i > -1; i--){
            inputWithoutSpace = deleteElement(inputWithoutSpace, spacesToDeleteIndexes[i]);
        }

        for(int i = 0; i < inputWithoutSpace.length; i++){
            hasIncorrectSymbols = checkIfOperandHasIncorrectInput(inputWithoutSpace[i]);
            if(hasIncorrectSymbols){
                return INCORRECT_INPUT;
            }
        }
        if(inputWithoutSpace.length == 0){
            return INCORRECT_INPUT;
        }
        for(String charStr : inputWithoutSpace){
            operand += charStr;
        }
        return operand;
    }

    public String operatorTransformation(String userInput){
        String[] userInputSplited = userInput.split("");
        String[] inputWithoutSpace = userInputSplited;
        String operator;
        int[] spacesToDeleteIndexes = {};
        boolean hasIncorrectSymbols = false;
        for(int i = 0; i < userInputSplited.length; i++){
            if(userInputSplited[i].equals(SPACE)){
                spacesToDeleteIndexes = addElement(spacesToDeleteIndexes, i);
            }
        }
        for(int i = spacesToDeleteIndexes.length - 1; i > -1; i--){
            inputWithoutSpace = deleteElement(inputWithoutSpace, spacesToDeleteIndexes[i]);
        }
        for(int i = 0; i < inputWithoutSpace.length; i++){
            hasIncorrectSymbols = checkIfOperatorHasIncorrectInput(inputWithoutSpace[i]);
            if(hasIncorrectSymbols){
                return INCORRECT_INPUT;
            }
        }
        if(inputWithoutSpace.length != 1){
            return INCORRECT_INPUT;
        }
        operator = inputWithoutSpace[0];
        return operator;
    }

    private boolean checkIfOperandHasIncorrectInput(String charInStr){
        String[] allowableValues = {ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE};
        for(int i = 0; i < allowableValues.length; i++){
            if(charInStr.equals(allowableValues[i])){
                return false;
            }
        }
        return true;
    }

    private boolean checkIfOperatorHasIncorrectInput(String charInStr){
        String[] allowableValues = {PLUS, MINUS, DIVIDE, MULTIPLY, REMAINDER_OF_DIVISION};
        for(int i = 0; i < allowableValues.length; i++){
            if(charInStr.equals(allowableValues[i])){
                return false;
            }
        }
        return true;
    }

    private int[] addElement(int[] array, int element){
        int arrayLength = array.length;
        int newLength = arrayLength + 1;
        int[] newArray = new int[newLength];
        for(int indexOfArray = 0; indexOfArray < arrayLength; indexOfArray++){
            newArray[indexOfArray] = array[indexOfArray];
        }
        newArray[newLength - 1] = element;
        return newArray;
    }

    private String[] deleteElement(String[] array, int index){
        int arrayLength = array.length;
        int newLength = arrayLength - 1;
        String[] newArray = new String[newLength];
        int tempIndex = 0;
        for(int indexOfArray = 0; indexOfArray < arrayLength; indexOfArray++){
            if(indexOfArray != index){
                newArray[tempIndex] = array[indexOfArray];
                tempIndex++;
            }
        }
        return newArray;
    }
}
