package main.java.com.calculator.services;

import main.java.com.calculator.utils.InputValidation;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class CalculatorService extends JFrame{
    private static final String INCORRECT_INPUT = "Ввод некорректный";
    private static final String PLUS = "+";
    private static final String MINUS = "-";
    private static final String DIVIDE = "/";
    private static final String MULTIPLY = "*";
    private static final String REMAINDER_OF_DIVISION = "%";

    InputValidation inputValidation;
    private int windowWidth = 400;
    private int windowHeight = 600;

    JLabel operand1Label = new JLabel("Первый операнд:");
    JLabel operand2Label = new JLabel("Второй операнд:");
    JLabel operatorLabel = new JLabel("Оператор:");
    JTextField operand1TextField;
    JTextField operand2TextField;
    JTextField operatorTextField;
    JTextField outputTextField;
    JButton startButton;

    public CalculatorService(JTextField operand1TextField, JTextField operand2TextField,
                             JTextField operatorTextField, JTextField outputTextField, JButton startButton,
                             InputValidation inputValidation){
        this.operand1TextField = operand1TextField;
        this.operand2TextField = operand2TextField;
        this.operatorTextField = operatorTextField;
        this.outputTextField = outputTextField;
        this.startButton = startButton;
        this.inputValidation = inputValidation;
    }

    public void run(){
        this.setName("Калькулятор");
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension dimension = toolkit.getScreenSize();
        this.setBounds(dimension.width / 2 - windowWidth / 2, dimension.height / 2 - windowHeight / 2, windowWidth, windowHeight);
        Container container = this.getContentPane();
        container.setLayout(new GridLayout(8, 1, 5, 5));

        container.add(operand1Label);
        container.add(operand1TextField);
        container.add(operand2Label);
        container.add(operand2TextField);
        container.add(operatorLabel);
        container.add(operatorTextField);
        startButton.setText("Вычислить");
        startButton.addActionListener(new StartButtonListener());
        container.add(startButton);
        container.add(outputTextField);
    }

    public class StartButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent event){
            String operand1Str = operand1TextField.getText();
            String operand2Str = operand2TextField.getText();
            String operatorStr = operatorTextField.getText();
            operand1Str = inputValidation.operandTransformation(operand1Str);
            operand2Str = inputValidation.operandTransformation(operand2Str);
            operatorStr = inputValidation.operatorTransformation(operatorStr);
            String resultStr = "Что-то пошло не так";

            if(operand1Str.equals(INCORRECT_INPUT) | operand2Str.equals(INCORRECT_INPUT) | operatorStr.equals(INCORRECT_INPUT)){
                resultStr = INCORRECT_INPUT;
            }
            else{
                int operand1 = Integer.parseInt(operand1Str);
                int operand2 = Integer.parseInt(operand2Str);

                Integer result;
                switch(operatorStr){
                    case PLUS:{
                        result = operand1 + operand2;
                        resultStr = result.toString();
                        break;
                    }
                    case MINUS:{
                        result = operand1 - operand2;
                        resultStr = result.toString();
                        break;
                    }
                    case DIVIDE:{
                        if (operand2 == 0){
                            resultStr = INCORRECT_INPUT;
                        }
                        else {
                            result = operand1 / operand2;
                            resultStr = result.toString();
                        }
                        break;
                    }
                    case MULTIPLY:{
                        result = operand1 * operand2;
                        resultStr = result.toString();
                        break;
                    }
                    case REMAINDER_OF_DIVISION:{
                        result = operand1 % operand2;
                        resultStr = result.toString();
                        break;
                    }
                }
            }
            outputTextField.setText(resultStr);
        }
    }

}
